package main

import (
	"fmt"
	hw_0 "gitlab.com/su-fmi-pecata/ai/hw/hw-0"
	hw_1 "gitlab.com/su-fmi-pecata/ai/hw/hw-1"
	hw_2 "gitlab.com/su-fmi-pecata/ai/hw/hw-2"
	hw_3 "gitlab.com/su-fmi-pecata/ai/hw/hw-3"
	hw_5 "gitlab.com/su-fmi-pecata/ai/hw/hw-5"
	hw_6 "gitlab.com/su-fmi-pecata/ai/hw/hw-6"
	hw_7 "gitlab.com/su-fmi-pecata/ai/hw/hw-7"
)

func main() {
	fmt.Printf("Run task ")
	taskNumber := 0
	_, _ = fmt.Scanf("%d", &taskNumber)
	fmt.Println("Running task ", taskNumber)

	switch taskNumber {
	case 0:
		hw_0.Solve()
		return
	case 1:
		hw_1.Solve()
		return
	case 2:
		hw_2.Solve()
		return
	case 3:
		hw_3.Solve()
		return
	case 5:
		hw_5.Solve()
		return
	case 6:
		hw_6.Solve()
		return
	case 7:
		hw_7.Solve()
		return
	default:
		fmt.Println("No such task ", taskNumber)
	}

}
