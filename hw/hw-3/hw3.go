package hw_3

import (
	"fmt"
	"gitlab.com/su-fmi-pecata/ai/hw"
)

func isPointWithNoCollision(m *hw.Matrix, row int, col int) bool {
	for c := 0; c < col; c++ {
		if (*m)[row][c] != 0 {
			return false
		}
	}

	for i := 1; row-i >= 0 && col-i >= 0; i++ {
		if (*m)[row-i][col-i] != 0 {
			return false
		}
	}

	for i := 1; row+i < m.RowSize() && col-i >= 0; i++ {
		if (*m)[row+i][col-i] != 0 {
			return false
		}
	}

	return true
}

func findQueens(m *hw.Matrix, n int, col int) bool {
	if col >= n {
		return true
	}

	for row := 0; row < n; row++ {
		if isPointWithNoCollision(m, row, col) {
			(*m)[row][col] = 1
			if findQueens(m, n, col+1) {
				return true
			}
			(*m)[row][col] = 0
		}

	}

	return false
}

func Solve() {
	var n int
	_, _ = fmt.Scanf("%d", &n)

	m := hw.CreateMatrix(n, n)

	if !findQueens(&m, n, 0) {
		fmt.Println("No solution found")
		return
	}

	m.Print()
}
