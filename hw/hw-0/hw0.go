package hw_0

import (
	"gitlab.com/su-fmi-pecata/ai/hw"
)

func bfs(inputData hw.InputData) []hw.Point {
	visited := hw.PathMap{}
	visited[inputData.StartPoint] = inputData.StartPoint

	waitList := []hw.Point{inputData.StartPoint}
	found := false
	for len(waitList) > 0 && !found {
		explorePoint := waitList[0]
		waitList = waitList[1:]

		possibleNewPoints := explorePoint.GetPossibleNewPoints(inputData.Matrix.RowSize(), inputData.Matrix.ColSize())
		for _, np := range possibleNewPoints {
			if inputData.Matrix.GetValue(np) <= 0 {
				continue
			}

			if _, hasBeenExplored := visited[np]; hasBeenExplored {
				continue
			}

			visited[np] = explorePoint
			waitList = append(waitList, np)
			found = found || (np == inputData.EndPoint)
		}
	}

	if !found {
		return []hw.Point{}
	}

	return visited.ReconstructPath(inputData.EndPoint)
}

func Solve() {
	inputData := hw.GetInputDataHw0And1()
	inputData.Matrix.PrintPath(bfs(inputData))
}
