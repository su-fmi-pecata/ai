package hw

import (
	"fmt"
	"math"
)

func AbsDiff(a int, b int) int {
	return int(math.Abs(float64(a - b)))
}

func GetInputDataHw0And1() InputData {
	var matrixSize int
	_, _ = fmt.Scanf("%d", &matrixSize)
	m := CreateMatrix(matrixSize, matrixSize)
	m.Read()

	startPoint := Point{0, 0}
	startPoint.Read()

	var endPoint Point
	endPoint.Read()

	return InputData{m, startPoint, endPoint}
}
