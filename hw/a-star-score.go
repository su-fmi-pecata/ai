package hw

type AStarScore struct {
	CostFromStart      int
	ExpectedCostToGoal int
}

func (a AStarScore) IsBefore(b AStarScore) bool {
	as := a.CostFromStart + a.ExpectedCostToGoal
	bs := b.CostFromStart + b.ExpectedCostToGoal
	return as < bs
}
