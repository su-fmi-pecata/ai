package hw

type Fraction struct {
	Numerator   int
	Denominator int
}

func (f Fraction) compare(b Fraction) int {
	return f.Numerator*b.Denominator - b.Numerator*f.Denominator
}
