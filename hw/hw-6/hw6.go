package hw_6

import (
	"encoding/csv"
	"fmt"
	"io"
	"math"
	"os"
	"sort"
	"strconv"
)

type flowerType string
type propertyType float64
type irisType struct {
	flower     flowerType
	properties []propertyType
}
type irisDistance struct {
	iris     irisType
	distance float64
}

const numberOfRowsInFile = 135
const numberOfColsInFile = 5
const propertiesLength = numberOfColsInFile - 1

func (i *irisType) distance(a *irisType) float64 {
	squareDistance := 0.0
	for ind := 0; ind < propertiesLength; ind++ {
		squareDistance += sqrDifference(i.properties[ind], a.properties[ind])
	}
	return math.Sqrt(squareDistance)
}

func sqrDifference(a, b propertyType) float64 {
	d := float64(a - b)
	return d * d
}

func parseProperty(d string) propertyType {
	v, e := strconv.ParseFloat(d, 64)
	if e != nil {
		panic("Cannot parse " + d)
	}
	return propertyType(v)
}

func getData() []irisType {
	f, err := os.Open("hw/hw-6/data.csv")
	if err != nil {
		panic(err)
	}
	defer f.Close()

	csvReader := csv.NewReader(f)
	data := make([]irisType, numberOfRowsInFile) // magic number of rows
	index := 0
	for {
		line, err := csvReader.Read()
		if err == io.EOF {
			break
		}
		if err != nil {
			panic(err)
		}

		properties := make([]propertyType, propertiesLength)
		for i := 0; i < propertiesLength; i++ {
			properties[i] = parseProperty(line[i])
		}

		data[index] = irisType{flower: flowerType(line[4]), properties: properties}
		index++
	}

	return data
}

func separateData(fullData []irisType) (dataSet []irisType, testData []irisType) {
	const module = 10
	const moduleResult = 3

	for ind, d := range fullData {
		if ind%module == moduleResult {
			testData = append(testData, d)
		} else {
			dataSet = append(dataSet, d)
		}
	}

	return
}

func kNN(dataSet []irisType, newIris irisType, k int) flowerType {
	distances := make([]irisDistance, len(dataSet))

	for index, iris := range dataSet {
		d := iris.distance(&newIris)
		distances[index] = irisDistance{iris: iris, distance: d}
	}

	sort.SliceStable(distances, func(a, b int) bool {
		return distances[a].distance < distances[b].distance
	})

	typesCount := make(map[flowerType]int)
	for i := 0; i < k; i++ {
		flower := distances[i].iris.flower
		typesCount[flower]++
	}

	// some magic to get any type from the map
	var closestType flowerType
	for closestType = range typesCount {
		break
	}

	for t, count := range typesCount {
		if count > typesCount[closestType] {
			closestType = t
		}
	}

	return closestType
}

func Solve() {
	dataSet, testData := separateData(getData())

	testDataLen := float64(len(testData))
	for k := 7; k < 53; k++ {
		correct := 0
		for _, iris := range testData {
			guessedType := kNN(dataSet, iris, k)
			if guessedType == iris.flower {
				correct++
			}
		}
		fmt.Println("k =", k, "SuccessRatio:", float64(correct)/testDataLen)
	}
}
