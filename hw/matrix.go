package hw

import "fmt"

type MatrixCell int8
type MatrixRow []MatrixCell
type Matrix []MatrixRow
type MatrixManipulator func(*Matrix, Point)

func CreateMatrix(rows int, cols int) Matrix {
	m := make(Matrix, rows)
	for i := range m {
		m[i] = make(MatrixRow, cols)
	}
	return m
}

func (matrix *Matrix) Copy() Matrix {
	m := CreateMatrix(matrix.RowSize(), matrix.ColSize())
	m.Manipulate(func(_ *Matrix, p Point) {
		m[p.Row()][p.Col()] = matrix.GetValue(p)
	})
	return m
}

func (matrix *Matrix) Manipulate(manipulate MatrixManipulator) {
	for indexRow, row := range *matrix {
		for indexCol, _ := range row {
			manipulate(matrix, Point{
				X: indexCol,
				Y: indexRow,
			})
		}
	}
}

func (matrix *Matrix) Read() {
	var tmp MatrixCell
	matrix.Manipulate(func(m *Matrix, point Point) {
		_, _ = fmt.Scanf("%d", &tmp)
		(*m)[point.Row()][point.Col()] = tmp
	})
}

func (matrix Matrix) Print() {
	for _, row := range matrix {
		fmt.Println(row)
	}
}

func (matrix Matrix) RowSize() int {
	return len(matrix)
}

func (matrix Matrix) ColSize() int {
	return len(matrix[0])
}

func (matrix Matrix) TotalSize() int {
	return matrix.RowSize() * matrix.ColSize()
}

func (matrix Matrix) PrintPath(path []Point) {
	if path == nil || len(path) < 1 {
		fmt.Printf("No path found")
		return
	}

	points := map[Point]bool{}
	for _, p := range path {
		points[p] = true
	}

	matrix.Manipulate(func(m *Matrix, p Point) {
		if p.Col() == 0 {
			fmt.Print("\n")
		}

		if _, hasBeenExplored := points[p]; hasBeenExplored {
			fmt.Printf("* ")
		} else {
			fmt.Printf("%d ", (*m)[p.Row()][p.Col()])
		}

	})
}

func (matrix Matrix) GetValue(point Point) MatrixCell {
	return matrix[point.Row()][point.Col()]
}
