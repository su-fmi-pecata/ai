package hw_1

import (
	"gitlab.com/su-fmi-pecata/ai/hw"
	"sort"
)

func getExpectedCostToGoal(point hw.Point, goal hw.Point) int {
	return point.GetManhattanDistance(goal)
}

func contains(s []hw.Point, e hw.Point) bool {
	for _, a := range s {
		if a == e {
			return true
		}
	}
	return false
}

func aStar(inputData hw.InputData) []hw.Point {
	openSet := []hw.Point{inputData.StartPoint}
	pathMap := hw.PathMap{inputData.StartPoint: inputData.StartPoint}
	scores := map[hw.Point]hw.AStarScore{
		inputData.StartPoint: {
			CostFromStart:      0,
			ExpectedCostToGoal: getExpectedCostToGoal(inputData.StartPoint, inputData.EndPoint),
		},
	}

	for len(openSet) > 0 {
		sort.SliceStable(openSet, func(a, b int) bool {
			sa, _ := scores[openSet[a]]
			sb, _ := scores[openSet[b]]
			return sa.IsBefore(sb)
		})

		explorePoint := openSet[0]
		openSet = openSet[1:]
		exploreScore := scores[explorePoint]

		possibleNewPoints := explorePoint.GetPossibleNewPoints(inputData.Matrix.RowSize(), inputData.Matrix.ColSize())
		for _, np := range possibleNewPoints {
			if inputData.Matrix.GetValue(np) <= 0 {
				continue
			}

			newPointScore, found := scores[np]
			newScore := hw.AStarScore{
				CostFromStart:      exploreScore.CostFromStart + 1,
				ExpectedCostToGoal: getExpectedCostToGoal(np, inputData.EndPoint),
			}

			if !found || (found && newScore.IsBefore(newPointScore)) {
				pathMap[np] = explorePoint
				scores[np] = newScore

				if !contains(openSet, np) {
					openSet = append(openSet, np)
				}
			}

			if inputData.EndPoint == np {
				return pathMap.ReconstructPath(inputData.EndPoint)
			}
		}
	}

	//
	return nil
}

func Solve() {
	inputData := hw.GetInputDataHw0And1()
	inputData.Matrix.PrintPath(aStar(inputData))
}
