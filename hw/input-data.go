package hw

type InputData struct {
	Matrix     Matrix
	StartPoint Point
	EndPoint   Point
}
