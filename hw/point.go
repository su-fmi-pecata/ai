package hw

import "fmt"

type Point struct {
	X int
	Y int
}

func (point *Point) Read() {
	_, _ = fmt.Scanf("%d", &point.X)
	_, _ = fmt.Scanf("%d", &point.Y)
}

func (point *Point) Print() {
	fmt.Printf("(r%d, c%d)", point.Row(), point.Col())
}

func (point Point) Row() int {
	return point.Y
}

func (point Point) Col() int {
	return point.X
}

func (point Point) GetPossibleNewPoints(matrixHeight, matrixWidth int) []Point {
	points := []Point{
		{point.X - 1, point.Y},
		{point.X, point.Y - 1},
		{point.X + 1, point.Y},
		{point.X, point.Y + 1},
	}

	var result []Point

	for _, np := range points {
		if np.X >= 0 && np.X < matrixWidth && np.Y >= 0 && np.Y < matrixHeight {
			result = append(result, np)
		}
	}
	return result
}

func (point Point) GetManhattanDistance(goal Point) int {
	return AbsDiff(point.Row(), goal.Row()) + AbsDiff(point.Col(), goal.Col())
}
