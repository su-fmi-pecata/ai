package hw

type PathMap map[Point]Point

func (pathMap PathMap) ReconstructPath(endPoint Point) []Point {
	var result []Point
	currentPoint := endPoint
	for true {
		prev, _ := pathMap[currentPoint]
		result = append([]Point{currentPoint}, result...)
		if currentPoint == prev {
			return result
		}
		currentPoint = prev
	}

	return []Point{}
}
