package hw_2

import (
	"fmt"
	"gitlab.com/su-fmi-pecata/ai/hw"
	"math"
	"sort"
	"strconv"
)

type state int
type expectedStateMapType map[hw.MatrixCell]hw.Point

const emptyCell = 0

func calculateState(m *hw.Matrix) state {
	val := 0
	numberSystem := m.TotalSize()
	m.Manipulate(func(matrix *hw.Matrix, p hw.Point) {
		val = val*numberSystem + int(matrix.GetValue(p))
	})
	return state(val)
}

func decomposeState(s state, squareSize int) hw.Matrix {
	ss := int(s)
	m := hw.CreateMatrix(squareSize, squareSize)
	numberSystem := m.TotalSize()
	for rowIndex := squareSize - 1; rowIndex >= 0; rowIndex-- {
		for colIndex := squareSize - 1; colIndex >= 0; colIndex-- {
			m[rowIndex][colIndex] = hw.MatrixCell(ss % numberSystem)
			ss = ss / numberSystem
		}
	}

	return m
}

func calculateExpectedCost(m hw.Matrix, expectedStateMap expectedStateMapType) int {
	cost := 0
	m.Manipulate(func(matrix *hw.Matrix, p hw.Point) {
		expectedPoint := expectedStateMap[m.GetValue(p)]
		cost = cost + p.GetManhattanDistance(expectedPoint)
	})
	return cost
}

func isSolvable(m hw.Matrix) bool {
	arr := make([]hw.MatrixCell, m.TotalSize())
	m.Manipulate(func(matrix *hw.Matrix, p hw.Point) {
		arr[p.Row()*m.RowSize()+p.Col()] = m.GetValue(p)
	})
	inversionsCount := 0
	for i := 0; i < m.TotalSize()-1; i++ {
		for ii := i + 1; ii < m.TotalSize(); ii++ {
			if arr[i] != emptyCell && arr[ii] != emptyCell && arr[i] > arr[ii] {
				inversionsCount++
			}
		}
	}
	return inversionsCount%2 == 0
}

func swapCells(m *hw.Matrix, a hw.Point, b hw.Point) {
	av := (*m)[a.Row()][a.Col()]
	(*m)[a.Row()][a.Col()] = (*m)[b.Row()][b.Col()]
	(*m)[b.Row()][b.Col()] = av
}

func findStartingPoint(matrix *hw.Matrix) hw.Point {
	var result hw.Point
	found := false
	matrix.Manipulate(func(m *hw.Matrix, p hw.Point) {
		if m.GetValue(p) == emptyCell {
			result = p
			found = true
		}
	})

	if !found {
		panic("No starting point found")
	}

	return result
}

func contains(s []state, e state) bool {
	for _, a := range s {
		if a == e {
			return true
		}
	}
	return false
}

func reconstructPath(stateMap map[state]state, s state, squareSize int) (solveSize int, path []hw.Point) {
	stepsCount := 0
	var result []hw.Point
	for true {
		prev, _ := stateMap[s]
		matrix := decomposeState(s, squareSize)
		point := findStartingPoint(&matrix)
		result = append([]hw.Point{point}, result...)
		if s == prev {
			return stepsCount, result
		}
		stepsCount = stepsCount + 1
		s = prev
	}

	return -1, nil
}

func aStar(matrix hw.Matrix, expectedStateMap expectedStateMapType) (solveSize int, path []hw.Point) {
	if !isSolvable(matrix) {
		return -1, nil
	}

	currentState := calculateState(&matrix)
	openSet := []state{currentState}
	stateMap := map[state]state{currentState: currentState}
	scores := map[state]hw.AStarScore{
		currentState: {
			CostFromStart:      0,
			ExpectedCostToGoal: calculateExpectedCost(matrix, expectedStateMap),
		},
	}

	if a, _ := scores[currentState]; a.ExpectedCostToGoal == 0 {
		return 0, nil
	}

	for len(openSet) > 0 {
		sort.SliceStable(openSet, func(a, b int) bool {
			sa, _ := scores[openSet[a]]
			sb, _ := scores[openSet[b]]
			return sa.IsBefore(sb)
		})

		exploreState := openSet[0]
		openSet = openSet[1:]
		exploreScore := scores[exploreState]

		matrix = decomposeState(exploreState, matrix.RowSize())
		emptyCellPoint := findStartingPoint(&matrix)
		possibleNewPoints := emptyCellPoint.GetPossibleNewPoints(matrix.RowSize(), matrix.ColSize())

		for _, np := range possibleNewPoints {
			newMatrix := matrix.Copy()
			swapCells(&newMatrix, emptyCellPoint, np)
			newState := calculateState(&newMatrix)
			newStateScore, found := scores[newState]
			newStateExpectedCostToGoal := calculateExpectedCost(newMatrix, expectedStateMap)
			newScore := hw.AStarScore{
				CostFromStart:      exploreScore.CostFromStart + 1,
				ExpectedCostToGoal: newStateExpectedCostToGoal,
			}

			if !found || (found && newScore.IsBefore(newStateScore)) {
				stateMap[newState] = exploreState
				scores[newState] = newScore

				if !contains(openSet, newState) {
					openSet = append(openSet, newState)
				}
			}

			if newStateExpectedCostToGoal == 0 {
				return reconstructPath(stateMap, newState, matrix.RowSize())
			}
		}
	}

	return -1, nil
}

func Solve() {
	var inputBlocks int
	_, _ = fmt.Scanf("%d", &inputBlocks)
	squareSize := int(math.Sqrt(float64(inputBlocks + 1)))
	if squareSize*squareSize != inputBlocks+1 || inputBlocks < 3 {
		panic("Not proper length " + strconv.Itoa(inputBlocks))
	}

	matrix := hw.CreateMatrix(squareSize, squareSize)
	expectedStateMap := expectedStateMapType{}
	matrix.Manipulate(func(matrix *hw.Matrix, p hw.Point) {
		cellValue := hw.MatrixCell(p.Row()*squareSize + p.Col() + 1)
		if squareSize == p.Row()+1 && p.Row() == p.Col() {
			cellValue = 0
		}
		expectedStateMap[cellValue] = p
	})
	matrix.Read()

	solveSize, path := aStar(matrix, expectedStateMap)
	fmt.Println(solveSize)
	if solveSize > 0 {
		p := path[0]
		step := ""
		for _, np := range path[1:] {
			if p.Row() == np.Row() {
				if p.Col() < np.Col() {
					step = "left"
				} else {
					step = "right"
				}
			} else {
				if p.Row() < np.Row() {
					step = "up"
				} else {
					step = "down"
				}
			}

			fmt.Println(step)
			p = np
		}
	}
}
