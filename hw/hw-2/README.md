# 8 Puzzle with A*

### Example
##### Input
```
8
1 2 3
5 6 0
7 8 4
```

##### Output
```
13
right
right
up
left
left
down
right
up
right
down
left
left
up
```
