package hw_7

import (
	"encoding/csv"
	"fmt"
	"io"
	"os"
)

type classType int8
type questionType int8
type answerType int8
type votesType map[questionType]answerType
type rowData struct {
	class classType
	votes votesType
}

type counterTypeClassesMap map[classType]int
type counterTypeVotesMap map[questionType]map[answerType]counterTypeClassesMap
type counterType struct {
	classes counterTypeClassesMap
	votes   counterTypeVotesMap
}

type candidateType struct {
	class classType
	prob  float64
}

const (
	skipped answerType = iota
	yes
	no
)

const (
	democrat classType = iota
	republican
)
const numberOfRowsInFile = 435
const numberOfColsInFile = 17
const foldCrossValidationNumber = 10

func createCounter() *counterType {
	return &counterType{
		classes: make(counterTypeClassesMap),
		votes:   make(counterTypeVotesMap),
	}
}

func (c *counterType) add(class classType, votes votesType) {
	c.classes[class]++
	for question, answer := range votes {
		if _, ok := c.votes[question]; !ok {
			c.votes[question] = make(map[answerType]counterTypeClassesMap)
		}
		if _, ok := c.votes[question][answer]; !ok {
			c.votes[question][answer] = make(counterTypeClassesMap)
		}

		c.votes[question][answer][class]++
	}
}

func (c *counterType) getSameVoteCount(class classType, question questionType, answer answerType) (int, bool) {
	if _, ok := c.votes[question]; !ok {
		return 0, false
	} else if _, ok := c.votes[question][answer]; !ok {
		return 0, false
	} else {
		return c.votes[question][answer][class], true
	}
}

func (c *counterType) totalParticipants() int {
	count := 0
	for _, v := range c.classes {
		count += v
	}
	return count
}

func (c *counterType) fillFrequencyTable(data []rowData, indexModuleToSkip int) {
	for index, row := range data {
		if index%foldCrossValidationNumber == indexModuleToSkip {
			continue
		}

		c.add(row.class, row.votes)
	}
}

func (c *counterType) naiveBayes(votes votesType) classType {
	out := make([]candidateType, len(c.classes))
	totalParticipants := float64(c.totalParticipants())
	for class, numberOfParticipants := range c.classes {
		classCountF := float64(numberOfParticipants)
		pc := classCountF / totalParticipants
		for question, answer := range votes {
			v, ok := c.getSameVoteCount(class, question, answer)
			if !ok {
				continue
			}

			pc *= float64(v) / totalParticipants
		}
		out = append(out, candidateType{class: class, prob: pc})
	}

	max := out[0]
	for _, candidate := range out[1:] {
		if max.prob < candidate.prob {
			max = candidate
		}
	}
	return max.class
}

func getData() []rowData {
	f, err := os.Open("hw/hw-7/data.csv")
	if err != nil {
		panic(err)
	}
	defer f.Close()

	csvReader := csv.NewReader(f)
	data := make([]rowData, numberOfRowsInFile) // magic number of rows
	index := 0
	for {
		line, err := csvReader.Read()
		if err == io.EOF {
			break
		}
		if err != nil {
			panic(err)
		}

		className := republican
		if line[0] == "democrat" {
			className = democrat
		}

		row := rowData{class: className, votes: make(votesType, numberOfColsInFile-1)}

		for i := 1; i < numberOfColsInFile; i++ {
			v := skipped
			if line[i] == "y" {
				v = yes
			} else if line[i] == "n" {
				v = no
			}
			row.votes[questionType(i)] = v
		}
		data[index] = row
		index++
	}

	return data
}

func Solve() {
	data := getData()
	fmt.Println("Data retrieved")

	results := make(map[int8]float64, foldCrossValidationNumber)
	for indexToSkip := 0; indexToSkip < foldCrossValidationNumber; indexToSkip++ {
		fmt.Println("Start executing ", indexToSkip)
		counter := createCounter()
		counter.fillFrequencyTable(data, indexToSkip)

		totalCount := 0
		successCount := 0

		for indexRow, row := range data {
			if indexRow%foldCrossValidationNumber != indexRow {
				continue
			}
			totalCount++
			class := counter.naiveBayes(row.votes)
			if class == row.class {
				successCount++
			}
		}

		results[int8(indexToSkip)] = float64(successCount*100) / float64(totalCount)
	}

	fmt.Println("Done")
	fmt.Println(results)

	total := 0.0
	for _, v := range results {
		total += v
	}
	fmt.Println("Combined: ", float64(total)/foldCrossValidationNumber)
}
