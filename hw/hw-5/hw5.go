package hw_5

import (
	"fmt"
	"gitlab.com/su-fmi-pecata/ai/hw"
	"math"
)

const gameSize = 3
const emptyCellMark = hw.MatrixCell(0)
const humanMark = hw.MatrixCell(1)
const computerMark = hw.MatrixCell(2)
const minVal = math.MinInt
const maxVal = math.MaxInt

func getShouldTheComputerStart() bool {
	const yes = "yes"
	const no = "no"

	var p string
	retry := true
	for retry {
		fmt.Printf("Should the computer start? (%s/%s)\n", yes, no)
		_, _ = fmt.Scanf("%s", &p)
		retry = p != yes && p != no
		if retry {
			fmt.Println("Oopps. Did not recognise ", p)
		}
	}

	return p == yes
}

func hasWonTheGame(m *hw.Matrix) (isWon bool, playerMark hw.MatrixCell) {
	// * * *
	// - - -
	// * * *
	for rowIndex := 0; rowIndex < m.RowSize(); rowIndex++ {
		row := (*m)[rowIndex]
		if emptyCellMark != row[0] && row[0] == row[1] && row[1] == row[2] {
			return true, row[0]
		}
	}

	// * * -
	// * * -
	// * * -
	for colIndex := 0; colIndex < m.ColSize(); colIndex++ {
		if emptyCellMark != (*m)[0][colIndex] && (*m)[0][colIndex] == (*m)[1][colIndex] && (*m)[1][colIndex] == (*m)[2][colIndex] {
			return true, (*m)[0][colIndex]
		}
	}

	// - * *
	// * - *
	// * * -
	if emptyCellMark != (*m)[1][1] && (*m)[0][0] == (*m)[1][1] && (*m)[1][1] == (*m)[2][2] {
		return true, (*m)[1][1]
	}

	// * * -
	// * - *
	// - * *
	if emptyCellMark != (*m)[1][1] && (*m)[2][0] == (*m)[1][1] && (*m)[1][1] == (*m)[0][2] {
		return true, (*m)[1][1]
	}

	return false, emptyCellMark
}

func areMoreTurnsAvailable(m *hw.Matrix) bool {
	availableTurns := 0
	m.Manipulate(func(m1 *hw.Matrix, p hw.Point) {
		if m.GetValue(p) == emptyCellMark {
			availableTurns++
		}
	})

	return availableTurns > 0
}

func isMovePossible(m *hw.Matrix, p hw.Point) bool {
	inRows := 0 <= p.Row() && p.Row() < m.RowSize()
	inCols := 0 <= p.Col() && p.Col() < m.ColSize()

	if !inRows || !inCols {
		return false
	}

	return m.GetValue(p) == emptyCellMark
}

func printMatrix(m *hw.Matrix) {
	fmt.Println()
	for row := 0; row < m.RowSize(); row++ {
		for col := 0; col < m.ColSize(); col++ {
			mark := "_"
			if (*m)[row][col] == humanMark {
				mark = "x"
			} else if (*m)[row][col] == computerMark {
				mark = "o"
			}
			fmt.Printf("%s ", mark)
		}
		fmt.Println()
	}
}

func printWinner(mc hw.MatrixCell) {
	fmt.Println()
	if mc == humanMark {
		fmt.Println("YOU WON")
	} else if mc == computerMark {
		fmt.Println("YOU LOSE")
	} else {
		fmt.Println("DRAW")
	}
}

func getPossibleMoves(m *hw.Matrix) []hw.Point {
	var moves []hw.Point
	m.Manipulate(func(m1 *hw.Matrix, p hw.Point) {
		if m1.GetValue(p) == emptyCellMark {
			moves = append(moves, p)
		}
	})

	return moves
}

func min(a, b int) int {
	if a < b {
		return a
	}
	return b
}

func max(a, b int) int {
	if a > b {
		return a
	}
	return b
}

func minmax(m *hw.Matrix, p hw.Point, isHumanPlayer bool, alpha int, beta int) int {
	mark := computerMark
	if isHumanPlayer {
		mark = humanMark
	}
	(*m)[p.Row()][p.Col()] = mark

	isWon, playerMark := hasWonTheGame(m)
	if !areMoreTurnsAvailable(m) {
		isWon = true
	}

	if isWon {
		(*m)[p.Row()][p.Col()] = emptyCellMark
		switch playerMark {
		case computerMark:
			return 10
		case emptyCellMark:
			return 0
		case humanMark:
			return -10
		}
	}

	moves := getPossibleMoves(m)
	var bestScore int
	if isHumanPlayer {
		bestScore = minVal
		for _, p := range moves {
			val := minmax(m, p, !isHumanPlayer, alpha, beta)
			bestScore = max(bestScore, val)
			alpha = max(alpha, bestScore)

			if alpha >= beta {
				break
			}
		}
	} else {
		bestScore = maxVal

		for _, p := range moves {
			val := minmax(m, p, !isHumanPlayer, alpha, beta)
			bestScore = min(bestScore, val)
			beta = min(beta, bestScore)

			if alpha >= beta {
				break
			}
		}
	}

	(*m)[p.Row()][p.Col()] = emptyCellMark
	return bestScore
}

func getComputerTurn(m *hw.Matrix) hw.Point {
	moves := getPossibleMoves(m)

	if len(moves) == m.TotalSize() {
		return hw.Point{X: m.ColSize() / 2, Y: m.RowSize() / 2}
	}

	var chosenOne hw.Point
	bestValue := minVal

	for _, p := range moves {
		val := minmax(m, p, false, minVal, maxVal)
		if val > bestValue {
			chosenOne = p
			bestValue = val
		}
	}

	return chosenOne
}

func Solve() {
	m := hw.CreateMatrix(gameSize, gameSize)
	isComputerTurn := getShouldTheComputerStart()
	var p hw.Point

	for true {
		if isComputerTurn {
			// play computer
			p = getComputerTurn(&m)
			m[p.Row()][p.Col()] = computerMark
		} else {
			// play human
			fmt.Println("\nIt's your turn")
			p.Read()
			for !isMovePossible(&m, p) {
				fmt.Println("Not possible move on ")
				p.Read()
			}
			m[p.Row()][p.Col()] = humanMark
		}

		printMatrix(&m)

		isComputerTurn = !isComputerTurn
		isWon, winnerMark := hasWonTheGame(&m)
		if !areMoreTurnsAvailable(&m) {
			isWon = true
		}

		if isWon {
			printWinner(winnerMark)
			return
		}
	}

}
